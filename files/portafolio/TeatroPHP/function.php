<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Proceso</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!--
	    ###########################################################
        # Aprendiz: Juan David Rojas Longas                       #
        # Programa de formación: Desarrollo Web con PHP (1660307) #
        # Evidencia: Taller "Uso de arreglos"                     #
        ###########################################################
	-->

	<style>
		/* Doy solo unos pocos estilos para la ventana emergente */
		body{
			text-align: center;
			width: 95%;
			margin: 0 auto;
		}
		p{
			margin-top: 10px;
			margin-bottom: 3px;
			padding: 10px;
		}
	</style>

</head>
<body>

<?php 
	# Incluyo como requerido el archivo biblioteca.php que es quien tiene el arreglo con la información en la tabla #
	require 'biblioteca.php';

	# Capturo la información ingresada por el usuario en el formulario. #
	# Le resto uno a el campo fila y puesto, pues el array comienza desde #
	# 0 y no estaría escogiendo el puesto y fila indicado por el usuario #
	$fila = $_POST['fila']-1; 
	$puesto = $_POST['puesto']-1;
	$valor = $_POST['valor'];

	$valorUser = $datos[$fila][$puesto];

	# creo la funcionalidad de los campos #
	if (($valorUser == "L") and ($valor == "L")) {
		echo "<p class='alert-danger'></b>Error</b><br />Este asiento está libre</p>";
	}elseif (($valorUser == "L") and ($valor == "R")) {
		echo "<p class='alert-success'><b>Excelente</b><br />Has reservado este asiento<br /><b>Fila: </b>".$_POST['fila']." || <b>Puesto: </b>".$_POST['puesto']."</p>";
	}elseif (($valorUser == "L") and ($valor == "V")) {
		echo "<p class='alert-success'><b>Excelente</b><br />Has comprado este asiento<br /><b>Fila: </b>".$_POST['fila']." || <b>Puesto: </b>".$_POST['puesto']."</p>";
	}

	if (($valorUser == "R") and ($valor == "L")) {
		echo "<p class='alert-success'><b>Excelente</b><br />Has liberado este asiento<br /><b>Fila: </b>".$_POST['fila']." || <b>Puesto: </b>".$_POST['puesto']."</p>";
	}elseif (($valorUser == "R") and ($valor == "R")) {
		echo "<p class='alert-danger'><b>Error</b><br />Este asiento actualmente está reservado.</p>";
	}elseif (($valorUser == "R") and ($valor == "V")) {
		echo "<p class='alert-success'><b>Excelente</b><br />Has comprado este asiento<br /><b>Fila: </b>".$_POST['fila']." || <b>Puesto: </b>".$_POST['puesto']."</p>";
	}

	if (($valorUser == "V") and ($valor == "L")) {
		echo "<p class='alert-danger'><b>Error</b><br />No puedes liberar este asiento, está vendido</p>";
	}elseif (($valorUser == "V") and ($valor == "R")) {
		echo "<p class='alert-danger'><b>Error</b><br />No puedes Reservar este puesto, está vendido</p>";
	}elseif (($valorUser == "V") and ($valor == "V")) {
		echo "<p class='alert-danger'><b>Error</b><br />Este asiento ya está comprado</p>";
	}

?>

<!-- Utilizo JavaScript para cerrar la ventana generada con el valor correspondiente -->
<a href="#" onclick="closeWindow()">Cerrar ventana</a>

<script>
	function closeWindow(){
		window.close();
	}
</script>

</body>
</html>