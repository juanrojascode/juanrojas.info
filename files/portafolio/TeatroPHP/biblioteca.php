
<?php

###########################################################
# Aprendiz: Juan David Rojas Longas                       #
# Programa de formación: Desarrollo Web con PHP (1660307) #
# Evidencia: Taller "Uso de arreglos"                     #
###########################################################

# Creo un array solo para colocar el número de las filas #
$numero = array(
    0 => '1', 
    1 => '2',
    2 => '3',
    3 => '4',
    4 => '5',
);

# Creo un array con el contenido de la tabla #
$datos = array(
    array(
        0 => 'R',
        1 => 'V',
        2 => 'V',
        3 => 'V',
        4 => 'R', 
        ),
    array(
        0 => 'L',
        1 => 'V',
        2 => 'L',
        3 => 'L',
        4 => 'L', 
        ),
    array(
        0 => 'L',
        1 => 'V',
        2 => 'R',
        3 => 'R',
        4 => 'R', 
        ),
    array(
        0 => 'V',
        1 => 'V',
        2 => 'V',
        3 => 'V',
        4 => 'V', 
        ),
    array(
        0 => 'L',
        1 => 'R',
        2 => 'R',
        3 => 'R',
        4 => 'R', 
    )
);

?>
