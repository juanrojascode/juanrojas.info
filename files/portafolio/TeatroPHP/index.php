<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Evidencia: Taller "Uso de formularios para transferencia" </title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Black+Han+Sans|Do+Hyeon|Gamja+Flower|Gugi" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">


    <style>
          body{
            background: radial-gradient(#2D54C3, #151B39);
          }
          h1{font-family: 'Do Hyeon', sans-serif; text-align: center;}
          .jumbotron{background-color: rgba(255,255,255,0.9); box-shadow: 0 0 28px black}
          .escenario{background-color: #dc3545; color: white; height: 50px; font-weight: bold; text-align: center; padding-top: 10px }
    </style>

    <?php
        ###########################################################
        # Aprendiz: Juan David Rojas Longas                       #
        # Programa de formación: Desarrollo Web con PHP (1660307) #
        # Evidencia: Taller "Uso de arreglos"                     #
        ###########################################################
        require 'biblioteca.php';
    ?>

</head>
<body>
    <div class="container mt-4">
        <div class="jumbotron">
          <h1 class="display-4 mb-4">Bienvenido al Teatro PHP</h1>
          <p class="lead mb-4 text-center">En este sitio podrás administrar los asientos del Teatro PHP.</p>
          <hr>
          <div class="row">
               <div class="col-lg-6">

                    <!--
                      Creo el formulario con los campos requeridos para su funcionalidad y según pide el cliente
                    -->
                    <form class="mt-5 mx-5" method="post" action="function.php" target="popup" onsubmit="window.open('', 'popup', 'width=270, height=110, top=100, left=250')">
                      <div class="form-group row">
                         <label class="col-sm-6 col-form-label">
                            Fila: 
                            <input type="text" name="fila" class="form-control" autofocus required>
                         </label>
                         <label class="col-sm-6 col-form-label">
                            Puesto: 
                            <input type="text" name="puesto" class="form-control">
                         </label>
                        <div class="col-sm-12 my-2 text-center">                          
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="valor" value="V">
                              <label class="form-check-label">Comprar</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="valor" value="R">
                              <label class="form-check-label">Reservar</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="valor" value="L">
                              <label class="form-check-label">Liberar</label>
                            </div>
                        </div>
                        <div class="col-sm-12 my-2 text-center" style="display: none">

                          <!--
                            Creo el contenido invisible con el contenido del array con la información
                            que se muentra en la tabla.
                          -->
                          <textarea name="oculto" id="oculto" cols="30" rows="10">
                            <?php 
                              echo json_encode($datos);
                            ?>
                          </textarea>
                        </div>
                      </div>
                      <div class="form-group row text-center">
                        <div class="col-sm-12">
                          <button type="submit" class="btn btn-primary" >Enviar</button>
                          <button type="reset" class="btn btn-danger">Borrar</button>
                        </div>
                      </div>
                    </form>
                    <p class="lead text-center">Creado por Juan Rojas</p>
               </div>
               <div class="col-lg-6 mt-4 px-5">
                    <div class="escenario rounded">Escenario</div>
                    <table class="table table-dark table-sm text-center mx-auto rounded-bottom" style="width: 80% !important">
                      <tbody>
                         <tr>
                          <th></th>
                          <th>1</th>
                          <th>2</th>
                          <th>3</th>
                          <th>4</th>
                          <th>5</th>
                         </tr>
                         <?php  

                              # Utilizo la función FOR para mostrar el contenido #
                              # hasta el final de arreglo #
                              for ($i=0; $i <count($datos) ; $i++) { ?>
                                <tr>
                                    <th><?php echo $numero[$i][0] ?></th>
                                    <td><?php echo $datos[$i][0] ?></td>
                                    <td><?php echo $datos[$i][1] ?></td>
                                    <td><?php echo $datos[$i][2] ?></td>
                                    <td><?php echo $datos[$i][3] ?></td>
                                    <td><?php echo $datos[$i][4] ?></td>
                                </tr>               
                            <?php }
                         ?>
                      </tbody>
                    </table>
               </div>
          </div>
        </div>
    </div>

</body>
</html>