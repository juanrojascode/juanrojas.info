<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">

	<title>Juan Rojas | Desarrollador web</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="icon" type="image/png" href="img/favi.png">
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="msapplication-TileImage" content="img/favi.png">
	<meta name="theme-color" content="#000000">

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/custom.css">
	
	<!-- Javascripts -->
	<script src="js/vendor/feather.min.js"></script>
	<script src="js/vendor/jquery-3.3.1.min.js"></script>
	<script src="js/vendor/popper.min.js"></script>

</head>
<body>
	<div id="social-lg">
		<div class="social">
			<a href="https://www.facebook.com/JuanRojasJD" class="wow fadeInLeft social-facebook" target="blank" data-wow-delay=".3s"><i data-feather="facebook"></i></a>
			<a href="https://www.instagram.com/juanrojasjd" class="wow fadeInLeft social-instagram" data-wow-delay=".4s"><i data-feather="instagram"></i></a>
		</div>
	</div>

	<div class="container">
		<div class="wrapper">
			<header class="navbar">
				<i data-feather="menu" data-toggle="collapse" data-target="#menu-responsive" aria-expanded="false" class="toggle-nav hidden-md"></i>
				<a href="#" class="wow fadeInLeft logo">Name</a>
				<nav id="menu-responsive" class="collapse show-md">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li><a class="wow fadeInDown active" data-wow-delay=".6s" data-toggle="tab" href="#perfil" role="tab" aria-selected="true"><i data-feather="user"></i>Inicio</a></li>
						<li><a class="wow fadeInDown" data-wow-delay=".5s" data-toggle="tab" href="#resumen" role="tab" aria-selected="false"><i data-feather="sliders"></i>Perfil</a></li>
						<li><a class="wow fadeInDown" data-wow-delay=".4s" data-toggle="tab" href="#servicios" role="tab" aria-selected="false"><i data-feather="layers"></i>Servicios</a></li>
						<li><a class="wow fadeInDown" data-wow-delay=".3s" data-toggle="tab" href="#portafolio" role="tab" aria-selected="false"><i data-feather="folder-plus"></i>Portafolio</a></li>
						<!-- <li><a class="wow fadeInDown" data-wow-delay=".2s" data-toggle="tab" href="#testimonio" role="tab" aria-selected="false"><i data-feather="users"></i>Testimonio</a></li> -->
						<li><a class="wow fadeInDown" data-wow-delay=".1s" data-toggle="tab" href="#contacto" role="tab" aria-selected="false"><i data-feather="mail"></i>Contacto</a></li>
					</ul>
				</nav>
			</header>
			<section class="mt-2 mb-2" id="tab-content">
				<article class="fade show active" id="perfil" role="tabpanel">
					<div class="image">
						<div class="container">
							<h1 class="mb-4 wow fadeInUpBig">Name<span>Desarrollador Web</span></h1>
							<div class="cuadroPadre wow rotateIn" data-wow-delay=".8s">
								<div class="cuadro"></div>
							</div>
							<div class="image-perfil wow zoomIn" data-wow-delay=".6s"></div>
						</div>
					</div>
					<div class="aboutme">
						<div class="container">
							<ul>
								<li class="wow zoomInUp adl-1"><i data-feather="user"></i><strong>Nombre:</strong>Juan David Rojas Longas</li>
								<li class="wow zoomInUp adm-1"><i data-feather="calendar"></i><strong>Nacimiento:</strong>Marzo 11 del 1995</li>
								<li class="wow zoomInUp adl-2"><i data-feather="map"></i><strong>Dirección:</strong>Nueva Castilla reservado, Bogotá D.C.</li>
								<li class="wow zoomInUp adm-2"><i data-feather="phone"></i><strong>Teléfono:</strong>(+57) 000 000 0000</li>
								<li class="wow zoomInUp adl-3"><i data-feather="mail"></i><strong>Correo:</strong>contacto@juanrojas.info</li>
							</ul>
							<h3 class="wow zoomInUp adm-3">Objetivo</h3>
							<p class="wow zoomInUp adl-4">Desarrollar contenido creativo y ser el mejor creando sitios web. Soy una persona creativa, espontánea, motivada a ser el mejor en el área de desarrollo web.</p>
							<p class="wow zoomInUp adm-4">Le doy la bienvenida a mi Corriculum Vitae, y le invito a navegar por el, esperando que sea de su agrado y que por supuesto estemos en contacto.</p>
						</div>
					</div>
				</article>

				<!-- Resumen -->
				<article class="fade" id="resumen" role="tabpanel">
					<div class="container">
						<div class="row mb-3">
							<div class="educacion wow fadeInRight">
									<div class="educacion-titulo">
										<i data-feather="book"></i>
										<h4>Educación</h4>
									</div>
									<ul>
										<li>
											<i class="icon"></i>
											<div class="educacion-instituto">
												<h5>Colegio Santa María</h5><span class="educacion-fecha">2009-2015</span>
											</div>
											<span>Girardot, Cundinamarca</span>
											<p>Cursado y aprobado la básica secundaria.</p>
										</li>
										<li>
											<i class="icon"></i>
											<div class="educacion-instituto">
												<h5>SENA</h5><span class="educacion-fecha">2016-2017</span>
											</div>
											<span>Girardot, Cundinamarca</span>
											<p>Tecnólogo en Diseño para la Comunicación Gráfica.</p>
										</li>
										<!--<li>
											<i class="icon"></i>
											<div class="educacion-instituto">
												<h5>Universidad Tadeo Lozano </h5><span class="educacion-fecha">2018</span>
											</div>
											<span>Bogotá D.C</span>
											<p>Diplomado en Desarrollo Web.</p>
										</li>-->
									</ul>
							</div>
							<div class="skills wow fadeInLeft" data-wow-delay="1s">
								<div class="skills-titulo">
									<i data-feather="book-open"></i>
									<h4>Habilidades</h4>
								</div>
								<ul>
									<li>
										<div class="progress">
											<div class="animated fadeInLeft progress-bar adm-1" style="width: 90%"></div>
										</div>
										<i class="icon"></i>
										<div class="skills-instituto">
											<h5>HTML5/CSS3</h5><span class="skills-fecha">90%</span>
										</div>
									</li>
									<li>
										<div class="progress">
											<div class="animated fadeInLeft progress-bar adm-2" style="width: 85%"></div>
										</div>
										<i class="icon"></i>
										<div class="skills-instituto">
											<h5>Diseño web</h5><span class="skills-fecha">85%</span>
										</div>
									</li>
									<li>
										<div class="progress">
											<div class="animated fadeInLeft progress-bar adm-3" style="width: 85%"></div>
										</div>
										<i class="icon"></i>
										<div class="skills-instituto">
											<h5>Diseño Gráfico</h5><span class="skills-fecha">85%</span>
										</div>
									</li>
									<li>
										<div class="progress">
											<div class="animated fadeInLeft progress-bar adm-4" style="width: 75%"></div>
										</div>
										<i class="icon"></i>
										<div class="skills-instituto">
											<h5>Desarrollo Web</h5><span class="skills-fecha">75%</span>
										</div>
									</li>
									<li>
										<div class="progress">
											<div class="animated fadeInLeft progress-bar adm-5" style="width: 90%"></div>
										</div>
										<i class="icon"></i>
										<div class="skills-instituto">
											<h5>Windows/Mac</h5><span class="skills-fecha">90%</span>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="empleo wow fadeInLeft" data-wow-delay="1.5s">
								<div class="empleo-titulo">
									<i data-feather="briefcase"></i>
									<h4>Empleos</h4>
								</div>
								<ul>
									<li>
										<i class="icon"></i>
										<div class="empleo-instituto">
											<h5>Segura Publicidad</h5><span class="empleo-fecha">2016-2017</span>
										</div>
										<span>Girardot, Cundinamarca</span>
										<p>Desarrollo de piezas gráficas publicitarias y elaboración/mantenimiento de sitios webs.</p>
									</li>
									<li>
										<i class="icon"></i>
										<div class="empleo-instituto">
											<h5>Legis S.A.</h5><span class="empleo-fecha">2017-actual</span>
										</div>
										<span>Bogotá D.C.</span>
										<p>Publicación de artículos en Ámbito Jurídico y Legis Móvil, elaboración y mantenimiento de sitios web.</p>
									</li>
								</ul>
							</div>
							<div class="certificados wow fadeInRight" data-wow-delay="2s">
								<div class="certificados-titulo">
									<i data-feather="award"></i>
									<h4>Certificados</h4>
								</div>
								<div class="empleo-scroll">
									<ul>
										<li>
											<i class="icon"></i>
											<div class="certificados-instituto">
												<h5>Diseño Web</h5><span class="certificados-fecha">2018</span>
											</div>
											<p>Certifica conocimientos en HTML5 y CCS3 para el desarrollo de sitios web. Conocimiento de Mobile First para crear sitios web responsives o adaptables a cualquier tipo de pantalla. Cursado en Udemy.com.</p>
										</li>
										<li>
											<div class="certificados-instituto">
												<h5>FlexBox</h5><span class="certificados-fecha">2018</span>
											</div>
											<p>Nueva metodología de diseño responsive. Cursado en Udemy.com</p>
										</li>
										<li>
											<div class="certificados-instituto">
												<h5>PHP</h5><span class="certificados-fecha">2018</span>
											</div>
											<p>Conocimientos básicos de programación PHP. SENA Virtual</p>
										</li>
										<!-- <li>
											<div class="certificados-instituto">
												<h5>PHP</h5><span class="certificados-fecha">2018</span>
											</div>
											<p>Conocimientos básicos de programación PHP. SENA Virtual</p>
										</li>
										<li>
											<div class="certificados-instituto">
												<h5>PHP</h5><span class="certificados-fecha">2018</span>
											</div>
											<p>Conocimientos básicos de programación PHP. SENA Virtual</p>
										</li> -->
									</ul>
								</div>
							</div>
						</div>
					</div>
				</article>

				<!-- Servicios -->
				<article class="fade" id="servicios" role="tabpanel">
					<div class="container">
						<div class="contenedor-servicios">
							<div class="card">
								<div class="card-descripcion">
									<div class="icon">
										<i data-feather="server"></i>
									</div>
									<div class="text-center">
										<h5 class="card-title">Hosting web</h5>
										<p class="card-text">Alojamiento ilimitado para su sitio web, podrá almacenar documentos, videos, fotografías y audios sin ningún límite de espacio</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-descripcion">
									<div class="icon">
										<i data-feather="slack"></i>
									</div>
									<div class="text-center">
										<h5 class="card-title">Dominio web</h5>
										<p class="card-text">Adquiera su dominio web a un bajo costo (.com .co .net)*</p>
										<p class="card-text">*Varía el costo entre dominios</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-descripcion">
									<div class="icon">
										<i data-feather="trending-up"></i>
									</div>
									<div class="text-center">
										<h5 class="card-title">SEO y Analytics</h5>
										<p class="card-text">Utilizando las mejores prácticas SEO para tener posicionamiento en motores de búsqueda, y junto con Google Analytics pordrá conocer la audiencia actual de su sitio web.</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-descripcion">
									<div class="icon">
										<i data-feather="smartphone"></i>
									</div>
									<div class="text-center">
										<h5 class="card-title">Responsive Design</h5>
										<p class="card-text">Se utiliza la metodología Mobile First y FlexBox para la total adaptación a cualquier dispositivo</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-descripcion">
									<div class="icon">
										<i data-feather="database"></i>
									</div>
									<div class="text-center">
										<h5 class="card-title">Diseño Web</h5>
										<p class="card-text">Se crea un diseño creativo teniendo en cuenta el público al que irá dirigido el sitio, teniendo en cuenta la usabilidad y armonidad visual</p>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-descripcion">
									<div class="icon">
										<i data-feather="database"></i>
									</div>
									<div class="text-center">
										<h5 class="card-title pl-2 pr-2">Alta calidad</h5>
										<p class="card-text">Todos los trabajos estarán bajo garatía, seguimiento y mantenimiento acordado por un tiempo</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</article>

				<!-- Portafolio -->
				<article class="fade" id="portafolio" role="tabpanel">
					<div class="container">
						<!-- <ul class="categorias">
							<li><a class="categoria active" href="#">Todos</a></li>
							<li><a class="categoria" href="#">Animación</a></li>
							<li><a class="categoria" href="#">Diseño</a></li>
							<li><a class="categoria" href="#">Ilustración</a></li>
							<li><a class="categoria" href="#">Fotografía</a></li>
							<li><a class="categoria" href="#">Música</a></li>
						</ul> -->
						<div class="gallery">
							<div class="gallery-item fotografia animacion">
								<div class="imagen">
									<div class="imagen-titulo">
										<h5>Letras<span>Corporeas</span></h5>
									</div>
									<i data-feather="chevron-up" class="arriba"></i>
								</div>
								<div class="contenedor">
									<h5>Letras Corporeas</h5>
									<p>Empresa especializada en realizar letras en diferentes materiales industriales.</p>
									<a href="http://www.letrascorporeas.co" class="btn btn-warning btn-sm" target="_blank">Visitar sitio</a>
								</div>
							</div>
							<div class="gallery-item diseno">
								<div class="imagen">
									<div class="imagen-titulo">
										<img src="img/portafolio/expert.png" alt="">
									</div>
									<i data-feather="chevron-up"></i>
								</div>
								<div class="contenedor">
									<h5>Expert Eventos</h5>
									<p>Empresa especializada en realizar eventos grupales, familiares y empresariales.</p>
									<a href="http://www.experteventos.com" class="btn btn-warning btn-sm" target="_blank">Visitar sitio</a>
								</div>
							</div>
							<div class="gallery-item musica animacion">
								<div class="imagen">
									<div class="imagen-titulo">
										<h5>Diseñadora</h5>
									</div>
									<i data-feather="chevron-up"></i>
								</div>
								<div class="contenedor">
									<h5>Diseñadora</h5>
									<p>Sitio web creado como curriculim vitae para una diseñadora gráfica.</p>
									<a href="portafolio/siteweb-slider/" class="btn btn-warning btn-sm" target="_blank">Visitar sitio</a>
								</div>
							</div>
							<div class="gallery-item musica animacion">
								<div class="imagen">
									<div class="imagen-titulo">
										<h5>Empresa</h5>
									</div>
									<i data-feather="chevron-up"></i>
								</div>
								<div class="contenedor">
									<h5>Empresa</h5>
									<p>Sitio web creado para una empresa ofreciendo sus productos.</p>
									<a href="portafolio/bootstrap/" class="btn btn-warning btn-sm" target="_blank">Visitar sitio</a>
								</div>
							</div>
							<div class="gallery-item fotografia diseno">
								<div class="imagen">
									<div class="imagen-titulo">
										<h5>Teatro en PHP</h5>
									</div>
									<p class="text-monospace">Prototipo</p>
									<i data-feather="chevron-up"></i>
								</div>
								<div class="contenedor">
									<h5>Teatro en PHP</h5>
									<p>Este prototipo se asemeja a la funcionalidad de un sistema de compra, venta y reserva de puestos en un teatro.</p>
									<a href="portafolio/TeatroPHP/" class="btn btn-warning btn-sm" target="_blank">Visitar sitio</a>
								</div>
							</div>
							<div class="gallery-item musica animacion opacity-0">
								<div class="imagen">
									<img src="img/portafolio/letrascorporeas.png" alt="">
									<i data-feather="chevron-up"></i>
								</div>
								<div class="contenedor">
									<p>Contenido</p>
								</div>
							</div>
						</div>
							<!-- <script src="js/vendor/pagination.js"></script> -->
					</div>
				</article>

				<!-- Testimonio 
				<article class="fade" id="testimonio" role="tabpanel">
					<div class="container">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit natus consequuntur pariatur maiores sunt, velit. Sit sunt, reprehenderit recusandae non expedita ipsam veritatis aliquid maxime iure. Inventore voluptates, optio provident.</p>
					</div>
				</article>-->

				<!-- Contacto -->
				<article class="fade" id="contacto" role="tabpanel">
						<div class="image">
							<div class="container">
								<figure>
									<img src="img/contacto.jpg" class="img-fluid" alt="">
								</figure>
							</div>
						</div>
						<div class="form-master">
							<div class="container">
								<h5>¿Qué tal le a parecido?</h5>
								<p>Espero que haya sido de su agrado, complete el formulario acontinuación para entrar en contacto e iniciar a trabajar juntos.</p>

								<form class='contacto' method='POST' action=''>
						            <div>
						                <input type="text" class="nombre form-control" name="nombre" placeholder="Nombre" value="<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>"><?php if(isset($errors)){ echo $errors[1]; } ?>
						            </div>

						            <div>
						                <input type="text" class="email form-control" name="email" placeholder="Correo Electrónico" value="<?php if(isset($_POST['email'])){ $_POST['email']; } ?>"><?php if(isset($errors)){ echo $errors[2]; } ?>
						            </div>

						            <div>
						                <input type="number" class="telefono form-control" name="telefono" placeholder="Número Telefónico" value="<?php if(isset($_POST['telefono'])){ $_POST['telefono']; } ?>"><?php if(isset($errors)){ echo $errors[3]; } ?>
						            </div>

						            <div>
						                <textarea rows="6" class="mensaje form-control" name="mensaje" placeholder="Ingrese su mensaje"><?php if(isset($_POST['mensaje'])){ $_POST['mensaje']; } ?></textarea>
						                <?php if(isset($errors)){ echo $errors[4]; } ?>
						            </div>

						            <button type="submit" class="btn float-right boton" name="boton">Submit</button>
						        </form>
							</div>
						</div>
				</article>
			</section>
			<div id="social-mx" class="mb-3">
				<div class="social">
					<a href="https://www.facebook.com/JuanRojasJD" target="_blank"><i data-feather="facebook"></i></a>
					<a href="https://www.instagram.com/juanrojasjd"><i data-feather="instagram"></i></a>
				</div>
			</div>
		</div>
	</div>

	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/vendor/wow.min.js"></script>
	<script src="js/custom.js"></script>

	<?php
	    if(isset($_POST['boton'])){
	        if($_POST['nombre'] == ''){
	            $errors[1] = '<span class="error">Ingrese su nombre</span>';
	        }else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
	            $errors[2] = '<span class="error">Ingrese un email correcto</span>';
	        }else if($_POST['telefono'] == ''){
	            $errors[3] = '<span class="error">Ingrese un número telefónico</span>';
	        }else if($_POST['mensaje'] == ''){
	            $errors[4] = '<span class="error">Ingrese un mensaje</span>';
	        }else{
	            $dest = "contacto@juanrojas.info"; //Email de destino
	            $nombre = $_POST['nombre'];
	            $email = $_POST['email'];
	            $asunto = "Ha escrito".$nombre." en JuanRojas.info"; //Asunto
	            $cuerpo = "
				            <!DOCTYPE html>
				            <html lang='es'>
				            <head>
				                <meta charset='UTF-8'>
				            </head>
				            <body>
				                <header>
				                    <h1>Ha escrito ".$nombre." en JuanRojas.info</h1>
				                </header>
				                <div>
				                    <P>Usuario: ".$nombre."</p>
				                    <p>Correo: ".$email."</p>
				                    <p>Teléfono: ".$_POST['telefono']."</p>
				                    <p>Mensaje: ".$_POST['mensaje']."</p> 
				                </div>
				            </body>
				            </html>"; //Cuerpo del mensaje
	            //Cabeceras del correo
	            $headers = "From: $nombre <$email>\r\n"; //Quien envia?
	            $headers .= "X-Mailer: PHP5\n";
	            $headers .= 'MIME-Version: 1.0' . "\n";
	            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; //
	 
	            if(mail($dest,$asunto,$cuerpo,$headers)){
	                $result = '<div id="contact_enviado" class="expand">
									<div class="enviado wow zoomInLeft">
									    <h4>¡Correo enviado!</h4>
									    <p>Gracias por contactarme, tendrá respuesta lo más pronto posible.</p>
									    <div class="cerrar">
									    	<a class="toggle-contact-enviado btn btn-warning btn-sm">Cerrar</a>
									    </div>
									</div>
							    </div>';
	                // si el envio fue exitoso reseteamos lo que el usuario escribio:
	                $_POST['nombre'] = '';
	                $_POST['email'] = '';
	                $_POST['telefono'] = '';
	                $_POST['mensaje'] = '';
	            }else{
	                $result = '<div class="result_fail">Hubo un error al enviar el mensaje </div>';
	            }
	        }
	    }

	    if(isset($result)) { echo $result; }
	?>

</body>
</html>