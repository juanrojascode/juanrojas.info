// Inicio iconos Feather
  feather.replace()

// Inicio WOW
new WOW().init();

// Animación tipo slider
$('a[data-toggle="tab"]').on('hide.bs.tab', function (e) {
    var $old_tab = $($(e.target).attr("href"));
    var $new_tab = $($(e.relatedTarget).attr("href"));

    if($new_tab.index() < $old_tab.index()){
        $old_tab.css('position', 'relative').css("right", "0").show();
        $old_tab.animate({"right":"-100%"}, 300, function () {
            $old_tab.css("right", 0).removeAttr("style");
        });
    }
    else {
        $old_tab.css('position', 'relative').css("left", "0").show();
        $old_tab.animate({"left":"-100%"}, 300, function () {
            $old_tab.css("left", 0).removeAttr("style");
        });
    }
});

$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    var $new_tab = $($(e.target).attr("href"));
    var $old_tab = $($(e.relatedTarget).attr("href"));

    if($new_tab.index() > $old_tab.index()){
        $new_tab.css('position', 'relative').css("right", "-2500px");
        $new_tab.animate({"right":"0"}, 500);
    }
    else {
        $new_tab.css('position', 'relative').css("left", "-2500px");
        $new_tab.animate({"left":"0"}, 500);
    }
});

// Active onClick categorias
var link = $('.categoria');

    link.on('click', function(){
      link.removeClass('active');
      $(this).addClass('active');
    });

//Toggle Cerrar/Abrir Menú Responsive
function toggleNavigation(){
    $('#menu-responsive').addClass('show');
    $('#menu-responsive').removeClass('show');
}

    $(window).on('load',function(){
        $('[data-toggle="tab"]').click(toggleNavigation);
    });

// Filtro Galeria
$(document).ready(function(){

  
    //comenzamos aplicandole un evento click a las etiquetas de tipo ancla
    //que se encuentran en la lista (todos, rojos, azules, amarillo)
    $('ul.categorias li a').click(function() {
        
        //el texto al cual dimos click lo pasamos a minusculas, 
        //le quitamos los espacios en blanco y lo asignamos a la variable
        //textoFiltro
        var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";        

        for (var i = 0; i < specialChars.length; i++) {
            var textoFiltro = $(this).text().toLowerCase().replace(new RegExp("\\" + specialChars[i], 'gi'), '')    
        } 

        //reemplaza caracteres especiales
        textoFiltro = textoFiltro.replace(/í/gi,"i");
        textoFiltro = textoFiltro.replace(/ó/gi,"o");
        textoFiltro = textoFiltro.replace(/ú/gi,"u");
        textoFiltro = textoFiltro.replace(/ñ/gi,"n");
        
        //si el texto es igual a 'todos' mostramos todos los elementos que contengan la clase hidden
        //y a dicho elemento le removemos la clase hidden
        //la clase hidden es opcional, en éste caso la usamos solo como referencia
        //puedes llamarla como quieras o incluso no utilizarla
        if(textoFiltro == 'todos') 
        {
            $('div.gallery div.hidden').fadeIn('slow').removeClass('hidden');

        }
        //de lo contrario hacemos lo siguiente
        else
        {
            //aqui empieza la magia
            
            //hacemos un bucle con el metodo each para
            //obtener todos los divs que se encuentren dentro de 
            //la clase gallery
            $('.gallery div').each(function() {
                
                //entonces comparamos
                //si el elemento NO tiene una clase llamada con el mismo valor que
                //nuestra variable textoFiltro, entonces se ocultará utilizando el metodo
                //fadeOut() de jQuery
                if(!$(this).hasClass(textoFiltro)) 
                {
                    $(this).fadeOut('normal').addClass('hidden');
                }
                //de lo contrario se mostrará utilizando el método
                //fadeIn() de jQuery
                else 
                {
                    $(this).fadeIn('slow').removeClass('hidden');
                }
            });
        }
        
        return false;
    });
});

//Toggle Cerrar Correo enviado
function toggleContactEnviado(){
    $('#contact_enviado').toggleClass('expand');
    $('#contact_enviado').toggleClass('collapse');
}

    $(window).on('load',function(){
        $('.toggle-contact-enviado').click(toggleContactEnviado);
    });

//Validación de Formulario
$(function() { 
    var emailreg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;   
    $(".boton").click(function(){  
        $(".error").fadeOut().remove();
        
        if ($(".nombre").val() == "") {  
            $(".nombre").focus().after('<span class="error">Ingrese su nombre</span>');  
            return false;  
        }  
        if ($(".email").val() == "" || !emailreg.test($(".email").val())) {
            $(".email").focus().after('<span class="error">Ingrese un email correcto</span>');  
            return false;  
        }  
        if ($(".telefono").val() == "") {  
            $(".telefono").focus().after('<span class="error">Ingrese un número telefónico</span>');  
            return false;  
        }  
        if ($(".mensaje").val() == "") {  
            $(".mensaje").focus().after('<span class="error">Ingrese un mensaje</span>');   
            return false;  
        }  
    });  
    $(".nombre, .telefono, .mensaje").bind('blur keyup', function(){  
        if ($(this).val() != "") {              
            $('.error').fadeOut();
            return false;  
        }  
    }); 
    $(".email").bind('blur keyup', function(){  
        if ($(".email").val() != "" && emailreg.test($(".email").val())) {  
            $('.error').fadeOut();  
            return false;  
        }  
    });
});