<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Juan Rojas | Curriculum Site</title>

	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/custom.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css">	

	<meta name="viewport" content="width=device-width, user-scalable=0">

	<meta name="keywords" content="Diseño gráfico, trabajos de diseño, portafolio, Diseñador Web, servicio de diseño gráfico">
	<meta name="description" content="Diseñador web especializado en UX/UI">
	<meta name="author" content="Juan Rojas">
</head>
<body>
	<header>
		<h1>
			<a href="#">Nombre</a>
		</h1>

		<nav id="iomenu">
			<figure id="showMovil" class="ioMenuMovil toggle-nav"><i class="fas fa-bars"></i></figure>

			<div id="card_menu" class="collapse">
				<figure id="hideMovil" class="ioMenuMovil toggle-nav"><i class="fas fa-times"></i></figure>

				<figure id="logo_menu">
					<i class="fab fa-grunt"></i>
				</figure>
				
				<ul>
					<li><a href="#" onclick="navSection('home')" class="toggle-nav">Home</a></li>
					<li><a href="#" onclick="navSection('sobre')" class="toggle-nav">Sobre mí</a></li>
					<li><a href="#" onclick="navSection('skills')" class="toggle-nav">Skills</a></li>
					<li><a href="#" onclick="navSection('porta')" class="toggle-nav">Portafolio</a></li>
					<li><a href="#" onclick="navSection('blog')" class="toggle-nav">Blog</a></li>
					<li><a href="#" class="toggle-nav toggle-contact">Contacto</a></li>
				</ul>
			</div>
		</nav>

		<div id="prev_section" onclick="displaySection('prev')"><i class="fas fa-angle-left"></i></div>
		<div id="next_section" onclick="displaySection('next')"><i class="fas fa-angle-right"></i></div>
	</header>

	<div id="slider">
		<section class="slide" id="home" style="display: block;">
			<div class="mainanimation">
				<div id="particles-js"></div>
			</div>
			<div class="mainlogo">
				<h2>Soy un <strong>Diseñador Gráfico</strong></h2>
				<figure><i class="fab fa-grunt"></i></figure>
			</div>
			<div class="mainbuttons">
				<div class="contactarme toggle-contact"><a href="#">Contactarme</a></div>
				<a href="#" class="descargarCv">Descargar CV</a>
			</div>
		</section>

		<section class="slide" id="sobre" style="display: none">
			<div id="content_aboutme">
				<figure>
					<img src="img/image.jpg" alt="Image girl by Freepik">
				</figure>

				<div id="aboutme">
					<div id="aboutme_content">
						<div class="aboutme_title">Sobre mi</div>
						<hr>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora, unde voluptatum quo enim doloremque ipsam cupiditate ex deleniti officiis. Distinctio suscipit quibusdam magni assumenda a nisi aliquid voluptatum in harum.</p>
						<p>Soluta officia a magni recusandae alias quo quia doloremque, enim iusto laudantium natus eveniet quae nam autem nihil reprehenderit esse cupiditate voluptatem ipsa quos aliquid saepe hic at, ratione incidunt.</p>
						<p>Ab accusantium beatae assumenda molestiae autem quisquam sit dolorem a, vitae iure inventore amet corrupti, cumque fugit similique aperiam porro animi voluptatem non facere. Ullam tempore mollitia maxime animi error!</p>
					</div>
				</div>
			</div>

			<div id="design_logo">
				<div class="row no-gutters">
					<div class="card col-sm-12 col-md-5 col-lg-2">
						<div class="card_img">
							<img src="https://picsum.photos/250/250" alt="">
						</div>
						<div class="card_title tag_a">
							<div class="title">
								<p><span>Diseño de</span> Logotipos</p>
								<div class="toggle-menu toggle-card"><span></span></div>
							</div>
							<div class="body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi natus libero repellendus fugiat nihil quam facilis harum ipsa nisi et error, quaerat dolores ipsum doloremque sunt enim amet ea illum.</p>
							</div>
						</div>
					</div>

					<div class="card col-sm-12 col-md-5 col-lg-2">
						<div class="card_img">
							<img src="https://picsum.photos/250/250" alt="">
						</div>
						<div class="card_title tag_b">
							<div class="title">
								<p><span>Diseño de</span> Logotipos</p>
								<div class="toggle-menu1 toggle-card1"><span></span></div>
							</div>
							<div class="body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi natus libero repellendus fugiat nihil quam facilis harum ipsa nisi et error, quaerat dolores ipsum doloremque sunt enim amet ea illum.</p>
							</div>
						</div>
					</div>

					<div class="card col-sm-12 col-md-5 col-lg-2">
						<div class="card_img">
							<img src="https://picsum.photos/250/250" alt="">
						</div>
						<div class="card_title tag_c">
							<div class="title">
								<p><span>Diseño de</span> Logotipos</p>
								<div class="toggle-menu2 toggle-card2"><span></span></div>
							</div>
							<div class="body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi natus libero repellendus fugiat nihil quam facilis harum ipsa nisi et error, quaerat dolores ipsum doloremque sunt enim amet ea illum.</p>
							</div>
						</div>
					</div>

					<div class="card col-sm-12 col-md-5 col-lg-2">
						<div class="card_img">
							<img src="https://picsum.photos/250/250" alt="">
						</div>
						<div class="card_title tag_d">
							<div class="title">
								<p><span>Diseño de</span> Logotipos</p>
								<div class="toggle-menu3 toggle-card3"><span></span></div>
							</div>
							<div class="body">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi natus libero repellendus fugiat nihil quam facilis harum ipsa nisi et error, quaerat dolores ipsum doloremque sunt enim amet ea illum.</p>
							</div>
						</div>
					</div>
				</div>
			</div>

		</section>

		<section class="slide" id="skills" style="display: none">
			<p>contenido 3</p>
		</section>

		<section class="slide" id="porta" style="display: none">
			<p>contenido 4</p>
		</section>

		<section class="slide" id="blog" style="display: none">
			<p>contenido 5</p>
		</section>
	</div>

	<div id="contact_form" class="collapse">
		<figure id="logo_form">
			<i class="far fa-envelope-open toggle-contact"></i>
		</figure>
		<div id="close_form">
			<i class="fas fa-times toggle-contact"></i>
		</div>
		<p><span>Dí</span> hola</p>	

		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="form1" name="form1">

			<input type="text" id="name" name="name" placeholder="Su Nombre">

			<input type="email" id="email" name="email" placeholder="Su Email">

			<textarea name="message" id="message" placeholder="Mensaje"></textarea>

			<input type="submit" name="submit" id="submit" value="Enviar">
		</form>
	</div>

	<div id="contact_button"><a href="#"><i class="fas fa-paper-plane toggle-contact"></i></a></span>
	</div>

	<footer>
		<div id="social">
			<a href=""><i class="fab fa-facebook-f"></i></a>
			<a href=""><i class="fab fa-twitter"></i></a>
			<a href=""><i class="fab fa-instagram"></i></a>
			<a href=""><i class="fab fa-behance"></i></a>
		</div>

		<p>Todos los derechos reservados | <a href="#">Políticas de privacidad</a></p>
	</footer>

	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
	<script type="text/javascript" src='js/particles.min.js'></script>
	<script type="text/javascript" src="js/particles.js"></script>

	<?php 
		$nombre = $_POST['name'];
		$mail = $_POST['email'];
		$mensaje = $_POST['message'];

		    if(isset($nombre) && !empty($nombre) &&
		        isset($mail) && !empty($mail) &&
		        isset($mensaje) && !empty($mensaje)){

		    	$para      = "ejemplo@experteventos.com";
				$titulo    = "Han escrito de tu sitio web";

				$cabeceras = "MIME-Version: 1.0\r\n";
			    $cabeceras .= "Content-type: text/html; charset = UTF-8\r\n";
			    $cabeceras .= "From: ".$mail."\r\n";

				$mensaje = "        
			            <!DOCTYPE html>
			            <html lang='en'>
			            <head>
			                <meta charset='UTF-8'>
			            </head>
			            <body>
			                <header>
			                    <h1>Correo</h1>
			                </header>
			                <div>
			                    <P>Usuario: ".$nombre."</p>
			                    <p>Correo: ".$mail."</p>
			                    <p>Mensaje: ".$mensaje."</p> 
			                </div>
			            </body>
			            </html>
			        ";

			    $enviado = mail($para, $titulo, $mensaje, $cabeceras);
		    }

			if($enviado){
		    echo "<div id='contact_enviado' class='expand'>
			    <i class='fas fa-times toggle-contact toggle-contact-enviado'></i>
			    <i class='fas fa-check toggle-contact-enviado'></i>
		    </div>";
			}

		?>
</body>
</html>