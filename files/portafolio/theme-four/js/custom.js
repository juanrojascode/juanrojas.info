//Toggle Cerrar/Abrir Menú Responsive
function toggleNavigation(){
	$('#card_menu').toggleClass('expand');
	$('#card_menu').toggleClass('collapse');
}

	$(window).on('load',function(){
		$('.toggle-nav').click(toggleNavigation);
	});

//Toggle Cerrar/abrir Formulario de Contacto
function toggleContact(){
	$('#contact_form').toggleClass('expand');
	$('#contact_form').toggleClass('collapse');
}

	$(window).on('load',function(){
		$('.toggle-contact').click(toggleContact);
	});

//Toggle Cerrar Correo enviado
function toggleContactEnviado(){
	$('#contact_enviado').toggleClass('expand');
	$('#contact_enviado').toggleClass('collapse');
}

	$(window).on('load',function(){
		$('.toggle-contact-enviado').click(toggleContactEnviado);
	});


//Toggle Aboutme
function toggleAboutMe() {
	$(document).on('click', '.toggle-card', function(){
		$(this).toggleClass('on');
		$('.tag_a').toggleClass('sube');
	});

	$(document).on('click', '.toggle-card1', function(){
		$(this).toggleClass('on');
		$('.tag_b').toggleClass('sube');
	});

	$(document).on('click', '.toggle-card2', function(){
		$(this).toggleClass('on');
		$('.tag_c').toggleClass('sube');
	});

	$(document).on('click', '.toggle-card3', function(){
		$(this).toggleClass('on');
		$('.tag_d').toggleClass('sube');
	});
}

toggleAboutMe();


//Toggle Funciones Slider
var sections = new Array(5);
	sections[0] = "home";
	sections[1] = "sobre";
	sections[2] = "skills";
	sections[3] = "porta";
	sections[4] = "blog";

var search;
var bkgr_body;
var bkgr_menu;
var show;

	function setColors(i){
		if (sections[i] == "home") {
			bkgr_body = "#233D58";
			bkgr_menu = "#233D58";
		}else{
			bkgr_body = "#f1f1f1";
			bkgr_menu = "#2196F3";
		}

		search = document.getElementById(sections[i]);
		search.style.background = bkgr_body;
		var menu = document.getElementsByTagName("header")[0];
		menu.style.background = bkgr_menu
	}

	function navSection(nav){
		for (var i=0; i<5; i++){
			search = document.getElementById(sections[i]);
			search.style.display = "none";

			if (sections[i] == nav) {
				search.style.display = "block";
				setColors(i)
			}
		}
	}

	function displaySection(nav){
		for (var i=0; i<5; i++) {
			search = document.getElementById(sections[i]);
			show = search.style.display;

			if (show == "block") {
				search.style.display = "none";

				if (nav == "next") {
					i++;
					if (i>4) i=0;
				}
				if (nav == "prev") {
					i--;
					if (i<0) i=4;
				}
				search = document.getElementById(sections[i]);
				search.style.display = "block";
				break;
			}
		}
		setColors(i)
	}