<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="author" content="Juan Rojas">
	<meta name="googlebot" content="noindex">
	<meta name="robots" content="noindex">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Bienvenido Juan Rojas</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
	<link rel="stylesheet" href="style.css">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
    

</head>
<body class="container tz-gallery">
		<?php
			$ruta = "img/"; // Indicar ruta
			 $filehandle = opendir($ruta); // Abrir archivos
			  while ($file = readdir($filehandle)) {
			   if ($file != "." && $file != "..") {
			    $tamanyo = GetImageSize($ruta . $file);
            ?>
<a class="lightbox" href="<?php echo $ruta.$file ?>"><img src="<?php echo $ruta.$file ?>" alt="Park"></a>
		<?php
		   } 
		  } 
		closedir($filehandle); // Fin lectura archivos
		?>
<script>baguetteBox.run('.tz-gallery');</script>
</body>
</html>