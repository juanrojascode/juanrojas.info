<?php
    if(isset($_POST['boton'])){
        if($_POST['nombre'] == ''){
            $errors[1] = '<div class="alert alert-danger error animated pulse">Obligatorio</div>';
        }else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
            $errors[2] = '<div class="alert alert-danger error animated pulse">Obligatorio</div>';
        }else if($_POST['telefono'] == ''){
            $errors[3] = '<div class="alert alert-danger error animated pulse">Obligatorio</div>';
        }else if($_POST['mensaje'] == ''){
            $errors[4] = '<div class="alert alert-danger error animated pulse">Obligatorio</div>';
        }else{
            $dest = "contacto@juanrojas.info"; //Email de destino
            $nombre = $_POST['nombre'];
            $email = $_POST['email'];
            $asunto = "Ha escrito".$nombre." en JuanRojas.info"; //Asunto
            $cuerpo = "
                        <!DOCTYPE html>
                        <html lang='es'>
                        <head>
                            <meta charset='UTF-8'>
                        </head>
                        <body>
                            <header>
                                <h1>Ha escrito ".$nombre." en JuanRojas.info</h1>
                            </header>
                            <div>
                                <P>Usuario: ".$nombre."</p>
                                <p>Correo: ".$email."</p>
                                <p>Teléfono: ".$_POST['telefono']."</p>
                                <p>Teléfono: ".$_POST['tipo']."</p>
                                <p>Mensaje: ".$_POST['mensaje']."</p> 
                            </div>
                        </body>
                        </html>"; //Cuerpo del mensaje
            //Cabeceras del correo
            $headers = "From: $nombre <$email>\r\n"; //Quien envia?
            $headers .= "X-Mailer: PHP5\n";
            $headers .= 'MIME-Version: 1.0' . "\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n"; //
    
            if(mail($dest,$asunto,$cuerpo,$headers)){
                $result = '
                <div id="contact_enviado" class="text-dark text-center">
                    <div class="enviado animated zoomIn">
                        <span class="h4">¡Correo enviado!</span>
                        <p class="mb-4">Gracias por contactarme, tendrá respuesta lo más pronto posible.</p>
                        <a class="contactCerrar d-block mx-auto btn btn-success text-light">Cerrar</a>
                    </div>
                </div>';
                // si el envio fue exitoso reseteamos lo que el usuario escribio:
                $_POST['nombre'] = '';
                $_POST['email'] = '';
                $_POST['telefono'] = '';
                $_POST['mensaje'] = '';
            }else{
                $result = '<div class="result_fail">Hubo un error al enviar el mensaje </div>';
            }
        }
    }

    if(isset($result)) { echo $result; }