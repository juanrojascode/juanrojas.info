<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sobre Mi - Juan Rojas || Desarrollador web</title>

    <?php
    include 'head.html'
    ?>
</head>
<body>
<div class="container-fluid animated fadeIn bg-sobremi">
    <?php
    include 'nav.html'
    ?>
    
    <section id="sobremi" class="text-light">
        <div class="contenedor wow jackInTheBox">
            <h3><span class="titulo">/</span> Sobre mi</h3>
            <p class="mt-2 mb-5 ml-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo quod excepturi, dolorem ad nisi, exercitationem ullam quisquam a nihil repellat assumenda et veritatis autem reprehenderit error maxime, dolor aut sint?</p>
        </div>

        <div class="contenedor wow jackInTheBox delay--5s">
            <h3><span class="titulo">/</span> Educación</h3>
            <div class="row mb-4 px-3">
                <div class="col-md-6 col-sm-12">
                    <div class="target">
                        <div class="edu-title">SENA <span>/ Gdot - Cund.</span></div>
                        <div class="edu-container">
                            <p>Diseñador gráfico</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="target">
                        <div class="edu-title">Col. Santa María <span>/ Gdot - Cund.</span></div>
                        <div class="edu-container">
                            <p>Educación media</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="contenedor wow jackInTheBox delay-1s">
            <h3><span class="titulo">/</span> Cursos en</h3>
            <ul class="cursos mb-5">
                <li>Diseño web avanzado</li>
                <li>Flexbox, mobile first</li>
                <li>PHP y MySQLi</li>
                <li>Javascript</li>
                <li>Drupal 7 theming</li>
                <li>Drupal 8</li>                
            </ul>
        </div>

        <div class="contenedor wow jackInTheBox delay-13s">
            <h3><span class="titulo">/</span> Empleos</h3>
            <div class="row mb-4 px-3">
                <div class="col-md-6 col-sm-12">
                    <div class="target">
                        <div class="edu-title">
                            <h6>LEGIS S.A. <span>/ Bogotá D.C.</span></h6>
                            <span class="float-right"><a href="https://www.legis.com.co" target="_blank">Ver sitio</a></span>
                        </div>
                        <div class="edu-container">
                            <p>Trabajo actualmente en esta empresa como Web Master, desarrollador y diseñador web, Administrado el contenido de <a href="https://www.ambitojuridico.com" target="_blank">Ámbito Jurídico</a> y <a href="https://www.legismovil.com">Legis Móvil</a>.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="target">
                        <div class="edu-title">
                            <h6>Segura publicidad <span>/ Gdot - Cund.</span></h6>
                            <span class="float-right"><a href="http://www.segurapublicidad.com" target="_blank">Ver sitio</a></span>
                        </div>
                        <div class="edu-container">
                            <p>Duré 1 año trabajando para esta empresa como Diseñador gráfico, elaborando piezas gráficas según requeria el cliente. También como desarrllador de páginas web y ofreciendo atención al cliente.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="contenedor wow jackInTheBox delay--5s">
            <h3><span class="titulo">/</span> Habilidades</h3>
            <div class="row mb-5 px-3">
                <div class="col-md-6 col-sm-12">
                    <div class="item">
                        <span class="h6">HTML/CSS</span>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 95%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">95%</div>
                        </div>
                    </div>
                    <div class="item">
                        <span class="h6">PHP / MySQLi</span>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 48%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">48%</div>
                        </div>
                    </div>
                    <div class="item">
                        <span class="h6">Adobe Illustrator</span>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 68%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">68%</div>
                        </div>
                    </div>
                    <div class="item">
                        <span class="h6">Adobe Photoshop</span>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 85%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">85%</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="item">
                        <span class="h6">Javascript</span>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 48%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">48%</div>
                        </div>
                    </div>
                    <div class="item">
                        <span class="h6">Adobe InDesign</span>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">50%</div>
                        </div>
                    </div>
                    <div class="item">
                        <span class="h6">Adobe After Effects</span>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 45%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">45%</div>
                        </div>
                    </div>
                    <div class="item">
                        <span class="h6">Windows / OS X</span>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 86%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">86%</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="contenedor wow jackInTheBox delay-1s">
            <h3><span class="titulo">/</span> Aptitudes</h3>
            <ul class="cursos mb-5">
                <li>Innovador</li>
                <li>Trabajo en equipo</li>
                <li>Atención al cliente</li>
                <li>Creatividad</li>
                <li>Dedicación</li>
                <li>Colaborador</li>
            </ul>
        </div><!-- End Conenedor -->
    </section>

</div>

    <?php
    include 'scripts.html'
    ?>
    
</body>
</html>