$(document).ready(function(){

    /*---------------------------------
    MENÚ MOBILE
    ----------------------------------*/
    var buttonToggle = $('#toggle');
    var buttonToggleClose = $('#toggleClose');
    var menuMobile = $('#menu');

    buttonToggle.click(function() {
        menuMobile.removeClass('d-none');
        menuMobile.addClass('fadeInLeft');
        buttonToggle.addClass('d-none');
        buttonToggleClose.removeClass('d-none');
    });

    buttonToggleClose.click(function() {
        menuMobile.removeClass('fadeInLeft');
        menuMobile.addClass('fadeOutLeft');

        setTimeout(() => {
            menuMobile.addClass('d-none');
            menuMobile.removeClass('fadeOutLeft');
        }, 500);

        buttonToggle.removeClass('d-none');
        buttonToggleClose.addClass('d-none');
    });

    /*---------------------------------
    CLOSE SENDING MESSAGE
    ----------------------------------*/
    $('.contactCerrar').click(function() {
        $('.enviado').removeClass('zoomIn');
        $('.enviado').addClass('zoomOut');

        setTimeout(() => {
            $('#contact_enviado').remove()
        }, 500);
    });

    /*------------------------
    INICIAMOS WOW
    -------------------------*/
    new WOW().init();

    /*---------------------------------
    SCROLL COLOR NAV
    ----------------------------------*/
    $(window).scroll(function () {
        var nav = $('.navbar');
        var scroll = $(window).scrollTop();

        if (scroll >= 10) {
            nav.addClass("fondo-navbar");
        } else {
            nav.removeClass("fondo-navbar");
        }
    });

    /*---------------------------------
    NAV ACTIVE TAG
    ----------------------------------*/
    var pathname = window.location.pathname;

    $("#menu a").each(function(){
        var href = $(this).attr('href');
        var indice = pathname.length - href.length;
        if (pathname.substring(indice) === href) {
            $(this).addClass('active')
        }
    })


    /*---------------------------------
    VALID FORM CONTACT
    ----------------------------------*/
    $(function() { 
        var emailreg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;   
        $(".boton").click(function(){  
            
            if ($(".nombre").val() == "") {  
                $(".nombre").focus().addClass('noValido');  
                return false;  
            }  
            if ($(".email").val() == "" || !emailreg.test($(".email").val())) {
                $(".email").focus().addClass('noValido');  
                return false;  
            }  
            if ($(".telefono").val() == "") {  
                $(".telefono").focus().addClass('noValido');  
                return false;  
            }  
            if ($(".mensaje").val() == "") {  
                $(".mensaje").focus().addClass('noValido');   
                return false;  
            }  
        });  
        $(".nombre, .telefono, .mensaje").bind('blur keyup', function(){  
            if ($(this).val() != "") {   
                $('.noValido').addClass('siValido');
                $('.noValido').removeClass('noValido');
                return false;  
            }  
        }); 
        $(".email").bind('blur keyup', function(){  
            if ($(".email").val() != "" && emailreg.test($(".email").val())) {  
                $('.noValido').addClass('siValido');
                $('.noValido').removeClass('noValido');
                return false;  
            }  
        });
    });


    /*---------------------------------
    Add '...' summary
    ----------------------------------*/
    $('.card-text p:nth-child(1)').append('...');
    $('.card-text p:nth-child(2)').remove();

})