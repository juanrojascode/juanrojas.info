<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Proyectos - Juan Rojas || Desarrollador web</title>

    <?php
    include 'head.html'
    ?>
</head>
<body>
<div class="container-fluid animated fadeIn bg-proyectos">
    <?php
    include 'nav.html'
    ?>
    
    <section id="proyectos" class="text-light">
        <p class="lead mb-5">Puede seleccionar entre las dos categorias existentes, si ver proyectos relacionados con desarrollo web o diseño gráfico.</p>
        <ul class="nav nav-pills mb-5" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a href="#pills-desarrollo" class="nav-link active" data-toggle="pill" role="tab" aria-controls="pills-desarrollo" aria-selected="true">Desarrollo Web</a>
            </li>
            <li class="nav-item">
                <a href="#pills-diseno" class="nav-link" data-toggle="pill" role="tab" aria-controls="pills-diseno" aria-selected="false">Diseño Gráfico</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-desarrollo" role="tabpanel" aria-labelledby="pills-desarrollo">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="card">
                            <a href="images/gallery/img1-lg.png" data-lightbox-title="" class="card-img-top">
                                <img src="images/gallery/img1.jpg" class="img-fluid"/>
                            </a>
                            <div class="card-body">
                                <p class="card-text">LETRAS CORPOREAS, es una página especializada en elaboración de letras en diferentes tipos de materiales en la ciudad de Girardot Cundinamarca.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="card">
                            <a href="images/gallery/img2-lg.jpg" data-lightbox-title="" class="card-img-top">
                                <img src="images/gallery/img2.jpg" class="img-fluid">
                            </a>
                            <div class="card-body">
                                <p class="card-text">EXPERT EVENTOS, es una empresa especializada en el desarrollo de eventos integrales de la ciudad de Girardot Cundinamarca.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="card">
                            <a href="images/gallery/img3-lg.jpg" data-lightbox-title="" class="card-img-top">
                                <img src="images/gallery/img3.jpg" class="img-fluid">
                            </a>
                            <div class="card-body">
                                <p class="card-text">Plantilla para una empresa anónima con animaciones.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="card">
                        <a href="images/gallery/img4.jpg" data-lightbox-title="" class="card-img-top">
                                <img src="images/gallery/img4.jpg" class="img-fluid">
                            </a>
                            <div class="card-body">
                                <p class="card-text">Desarrollo de página tipo slider horizontal.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="card">
                        <a href="images/gallery/img5-lg.gif" data-lightbox-title="" class="card-img-top">
                                <img src="images/gallery/img5.jpg" class="img-fluid">
                            </a>
                            <div class="card-body">
                                <p class="card-text">Diseño tipo tarjeta de presentación con páginas tipo slider horizontal.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-diseno" role="tabpanel" aria-labelledby="pills-diseno">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="card">
                        <a class="card-img-top" href="/SitioDos" lightbox="iframe" url="/SitioDos"  style="background: pink;">
                            <img src="#" class="img-fluid" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="card">
                        <a class="card-img-top" href="/SitioDos" lightbox="iframe" url="/SitioDos"  style="background: purple;">
                            <img src="#" class="img-fluid" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="card">
                        <a class="card-img-top" href="/SitioDos" lightbox="iframe" url="/SitioDos"  style="background: orange;">
                            <img src="#" class="img-fluid" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="card">
                        <a class="card-img-top" href="/SitioDos" lightbox="iframe" url="/SitioDos"  style="background: purple;">
                            <img src="#" class="img-fluid" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="card">
                        <a class="card-img-top" href="/SitioDos" lightbox="iframe" url="/SitioDos"  style="background: orange;">
                            <img src="#" class="img-fluid" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="card">
                        <a class="card-img-top" href="/SitioDos" lightbox="iframe" url="/SitioDos"  style="background: pink;">
                            <img src="#" class="img-fluid" alt="Card image cap">
                        </a>
                        <div class="card-body">
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
</div>
    <?php
    include 'scripts.html'
    ?>
    
</body>
</html>