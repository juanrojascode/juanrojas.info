<?php

    include_once 'admin/core/conexion.php';

    //LEER TABLA
    $sql_leer = 'SELECT * FROM articles ORDER BY fecha_publicacion DESC';
    $gsent = $pdo->prepare($sql_leer);
    $gsent->execute();
    $resultado = $gsent->fetchAll();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog - Juan Rojas || Desarrollador web</title>

    <?php
        include 'head.html'
    ?>
</head>
<body>
<div class="container-fluid animated fadeIn bg-contacto">
    <?php
        include 'nav.html'
    ?>
    
    <section id="blog" class="text-light">
        <div class="container">
        <div class="row">
            <?php
                foreach ($resultado as $dato):
            ?>
            <div class="col-md-<?php echo $dato['select_template']; ?> col-12 mb-4">
                <div class="card">
                    <img class="card-img-top" src="<?php echo $dato['ruta_imagen_home'] ?>" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $dato['title_node']; ?></h5>
                        <h6 class="card-subtitle mb-2 text-muted"><?php
                                $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                                $date = new DateTime( $dato['fecha_publicacion'] );
                                $result = $date->format('j');
                                $result1 = $meses[$date->format('n')-1];
                                $result2 = $date->format('Y');
                                echo $result.' de '.$result1.' del '.$result2;
                            ?></h6>
                        <div class="card-text">
                            <?php
                                $rest = substr($dato['content_node'], 0, 255);
                                echo $rest;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
        </div>            
        </div>
    </section>

</div>

    <?php
        include 'scripts.html'
    ?>
    
</body>
</html>