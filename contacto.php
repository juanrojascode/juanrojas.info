<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contacto - Juan Rojas || Desarrollador web</title>

    <?php
    include 'head.html'
    ?>
</head>
<body>
<div class="container-fluid animated fadeIn bg-contacto">
    <?php
    include 'nav.html'
    ?>
    
    <section id="contacto" class="text-light">
        <!-- <div class="container"> -->
            <p class="mt-2 mb-5 lead">Puede llenar el siguiente formulario para contactar y cotizar el desarrollo de un sitio web.</p>
            <p>*Para que le pueda brindar un mejor servicio, todos los campos son requeridos antes de enviar el correo.</p>
            <form class='contacto' method='POST' action=''>
                <div class="form-group">
                    <input type="text" class="nombre form-control form-control-lg" name="nombre" placeholder="Nombre" value="<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>"><?php if(isset($errors)){ echo $errors[1]; } ?>
                </div>

                <div class="form-group">
                    <input type="text" class="email form-control form-control-lg" name="email" placeholder="Correo Electrónico" value="<?php if(isset($_POST['email'])){ $_POST['email']; } ?>"><?php if(isset($errors)){ echo $errors[2]; } ?>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input type="number" class="telefono form-control form-control-lg" name="telefono" placeholder="Número Telefónico" value="<?php if(isset($_POST['telefono'])){ $_POST['telefono']; } ?>"><?php if(isset($errors)){ echo $errors[3]; } ?>
                    </div>
                    <div class="form-group col-md-6">
                        <select name="tipo" class="form-control">
                            <option>Cotización</option>
                            <option>Solicitud</option>
                            <option>Re-diseño</option>
                        </select>
                    </div>
                </div>                

                <div class="form-group">
                    <textarea rows="6" class="mensaje form-control form-control-lg" name="mensaje" placeholder="Ingrese su mensaje"><?php if(isset($_POST['mensaje'])){ $_POST['mensaje']; } ?></textarea>
                    <?php if(isset($errors)){ echo $errors[4]; } ?>
                </div>

                <button type="submit" class="mt-4 d-block mx-auto btn-lg btn-primary boton" name="boton"><i class="fas fa-paper-plane"></i> Enviar</button>
            </form>
        <!-- </div> -->

        <?php
        require 'post.php'
        ?>
    </section>

</div>

    <?php
    include 'scripts.html'
    ?>
    
</body>
</html>