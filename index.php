<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Juan Rojas || Desarrollador web</title>

    <?php
    include 'head.html'
    ?>

</head>
<body>
<div class="container-fluid animated fadeIn">
    <?php
    include 'nav.html'
    ?>
    
    <section id="home" class="text-light">
        <div id="particles-js"></div>
        <a href="files/hojadevida-juandavidrojaslongas.pdf" class="btn btn-warning mt-4 mb-5 shadow">Descargar HV</a>
        <figure class="rounded-circle">
            <img src="images/foto.jpg" class="img-fluid rounded-circle" width="200px" alt="">
        </figure>
        <span class="display-4 mb-3">¡Hola!</span>
        <p class="lead text-center mb-5 px-3">Soy Juan David Rojas y me gusta el diseño y la innovación.</p>
        <div class="social mb-4">
            <a href="https://www.facebook.com/JuanRojasJD" target="_blank"><i class="fab fa-facebook-f"></i></a>
            <a href="https://www.instagram.com/juanrojasjd/" class="_blank"><i class="fab fa-instagram"></i></a>
            <a href="https://twitter.com/JuanRojasJD" target="_blank"><i class="fab fa-twitter"></i></a>
            <a href="https://github.com/juanrojasjr" target="_blank"><i class="fab fa-github"></i></a>
            <a href="https://www.behance.net/juanrojas95" target="_blank"><i class="fab fa-behance"></i></a>
        </div>
        <a href="contacto" class="text-light font-weight-bold mb-4"><i class="fas fa-envelope"></i> contacto@juanrojas.info</a>
        <a href="sobremi" class="btn btn-dark mb-5 shadow">Ver habilidades</a>
    </section>
</div>

    <script src='js/particles.min.js'></script>
    <?php
    include 'scripts.html'
    ?>
    <script>
        particlesJS("particles-js",{particles:{number:{value:100,density:{enable:!0,value_area:800}},color:{value:"#cccccc"},shape:{type:"circle",polygon:{nb_sides:350}},size:{value:2,random:!0,anim:{enable:!1,speed:140,size_min:.1,sync:!1}},line_linked:{enable:!0,distance:150,color:"#cccccc",opacity:.4,width:1},move:{enable:!0,speed:1.5,direction:"none",random:!1,straight:!1,out_mode:"out",bounce:!1,attract:{enable:!1,rotateX:600,rotateY:1200}}},retina_detect:!0});
    </script>

    <!-- comentario -->
    
</body>
</html>