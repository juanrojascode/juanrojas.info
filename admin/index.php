<?php session_start();
    if( isset($_SESSION['admin'])){
        header('location:panel-admin/');
    }else{
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>JuanRojas || Login Admin</title>

    <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../css/animate.css">

    <style>
        body{
            background-image: url('../images/bg.jpg');
        }
        .contenedor{
            background: rgba(0,0,0,.5);
            box-shadow: 0px 0px 10px black;
            padding: 1rem 3rem;
            display: flex;
            flex-direction: column;
            align-items: center
        }
        input{
            background: transparent !important;
            box-shadow: 0px 0px 10px black;
            border-color: #013c7b !important;
            color: white;
            border-radius: 0 !important
        }
        .ingresar{
            margin: 0 auto;
            width: 80%;
            border-radius: 0
        }

        @media (min-width: 768px){
            .contenedor{
                padding: 5rem 8rem;
            }
        }
    </style>

</head>
<body class="bg-dark">
<div class="container animated fadeIn d-flex flex-column justify-content-center align-items-center text-light" style="height: 100vh">

    <div class="contenedor">
        <form action="core/login.php" method="POST" class="d-flex flex-column">
            <input type="text" name="nombre_usuario" placeholder="Usuario" class="clase mb-4 form-control">
            <input type="password" name="contrasena" placeholder="Contraseña" class="mb-4 form-control">
            <button type="submit" class="btn btn-primary ingresar">Ingresar</button>
        </form>
    </div>

</div>
</body>
</html>