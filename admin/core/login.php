<?php
session_start();

include_once 'conexion.php';

$usuario_login = $_POST['nombre_usuario'];
$contrasena_login = $_POST['contrasena'];


//VERIFICAR SI EL USUARIO EXISTE
$sql = 'SELECT * FROM useradmin WHERE username = ?';
$sentencia = $pdo->prepare($sql);
$sentencia->execute(array($usuario_login));
$resultado = $sentencia->fetch();

if(!$resultado){
    //matar la operación
    echo 'No existe el usuario';
    die();
}

if( password_verify($contrasena_login, $resultado['userpass']) ){
    //las contraseñas son igual
    $_SESSION['admin'] = $usuario_login;
    header('location: ../panel-admin/');
}else{
    echo 'No son iguales las contraseñas';
    die();
}