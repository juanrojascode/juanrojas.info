$(document).ready(function () {

    $(".parent a").click(function () {
        var t = $(this);
        //t.toggleClass("open");
        if ($(".parent a[data-toggle='collapse']").parent().children().hasClass("open") === true) {
            t.removeClass("open");
        }else{
             t.addClass("open");
         }
        
        // if ( t.hasClass("open") === true ) {
        //     t.removeClass("open");
        // }else{
        //     t.addClass("open");
        // }
    });

    $("#cMenu").click(function () {
        var sL = $("#sL");
        var bToggle = $("#cMenu");
            sL.toggleClass("sS");
            bToggle.toggleClass("cI")
    });

    var pathname = window.location.pathname;
    var hText = $(".page-title");
    if (pathname === "/admin/panel-admin/node" ) {
        hText.html("Crear artículo")
    }if (pathname === "/admin/panel-admin/editar") {
        hText.html("Editar artículo")
    }

    $('<div class="scroll-top"></div>').appendTo("body");

    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
             $('.scroll-top').fadeIn();
        } else {
             $('.scroll-top').fadeOut();
        }
     });

     $('.scroll-top').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    $(".cPublicado a.dropdown-item").click(function () { 
        var valor = $(this).attr("data-id");
        var vCompleto = "eliminar?id=" + valor;
        $(".modal .aceptar").attr("href",vCompleto);
    });

    function ActualizarHora(){
        var f = new Date();
        var s = f.getSeconds();
        var m = f.getMinutes();
        var h = f.getHours();
     
        var eHoras = document.getElementById("pHoras");
        var eMinutos = document.getElementById("pMinutos");
        var eSegundos = document.getElementById("pSegundos");
        var eSaludo = document.getElementById("pSaludo");
     
        eHoras.textContent = h;
        eMinutos.textContent = m;
        eSegundos.textContent = s;
        
        if (h >= 8 && m >= 1 && h < 12) {
            eSaludo.textContent = "Buenos días";
        }
        if (h >= 12 && m >= 1 && h < 19) {
            eSaludo.textContent = "Buenas tardes";
        }
        if (h >= 19 && m >= 1) {
            eSaludo.textContent = "Buenas noches";
        }
    }
    //setInterval(ActualizarHora,1000);
    
});