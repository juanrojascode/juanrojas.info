<?php session_start();
    if( isset($_SESSION['admin'])){

        include_once '../core/conexion.php';

        //LEER TABLA
        $sql_leer = 'SELECT * FROM articles';
        $gsent = $pdo->prepare($sql_leer);
        $gsent->execute();
        $resultado = $gsent->fetchAll();

        //AGREGAR
        if($_POST){
            $articleTitle = $_POST['title_node'];
            $articleContent = $_POST['content_node'];
            $articleTemplate = $_POST['select_template'];
            $articleDate = $_POST['fecha_creacion'];

            if($articleTitle === '' || $articleContent === '' || $articleTemplate === ''){
                echo 'error';
            }else{
                echo 'agregado';
            }

            if(isset($_FILES['img'])){
                $articleImage=$_FILES['img']['name'];
                $ruta=$_FILES['img']['tmp_name'];
                $destino="../../images/articles/".$articleImage;
                $destinoHome="images/articles/".$articleImage ;
                
                if(copy($ruta,$destino)){
                    $sql_agregar = 'INSERT INTO articles (title_node,content_node,select_template,ruta_imagen,ruta_imagen_home,fecha_publicacion) VALUES (?,?,?,?,?,?)';
                    $sentencia_agregar = $pdo->prepare($sql_agregar);
                    $sentencia_agregar->execute(array($articleTitle,$articleContent,$articleTemplate,$destino,$destinoHome,$articleDate));
                }
            }

            $sentencia_agregar = null;
            $pdo = null;
            header('location:index');
        }
    }else{
        header('location:../index');
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php
    include 'head.html'
    ?>

    <title>Crear Artículo || Panel Administrativo</title>

    <script src="../ckeditor/ckeditor.js"></script>
    
    <style>
    #editor1{
        height: 100%;
        min-height: 600px
    }
    </style>

</head>
<body>
    <div class="wrapper">

        <?php
        include 'nav.html'
        ?>

        <section class="content">
            <div class="main-content container-fluid">
                <form method="POST" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="card col-sm-12 col-md-9 card-border-color card-border-color-primary">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Título <span class="requerido">*</span></label>
                                    <input class="form-control" name="title_node">
                                </div>
                                <div class="form-group">
                                    <label>Contenido</label>
                                    <textarea name="content_node" id="editor1"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card col-sm-12 col-md-3 card-border-color card-border-color-primary">
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="mr-2 align-self-start">Tipo de Plantilla:</label>
                                    <div class="select-template mt-3 ml-2">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="select_template" value="6" checked>
                                            <label class="form-check-label"> <img src="images/layout1.svg" alt="Template grande" width="45px"> </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="select_template" value="4">
                                            <label class="form-check-label"><img src="images/layout2.svg" alt="Template mediano" width="45px"></label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="select_template" value="3">
                                            <label class="form-check-label"><img src="images/layout3.svg" alt="Template pequeño" width="45px"></label>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label>Seleccione la imagen de portada:</label> 
                                    <input type="file" name="img" class="form-control w-100">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label>Fecha de publicación:</label>
                                    <?php 
                                        $date = date('d-m-Y h:i:s');
                                        date_default_timezone_set('America/Bogota');
                                        echo '<input type="date" class="form-control" value="'.date('Y-m-d').'" name="fecha_creacion" id="date">';
                                    ?>
                                </div>
                                <button class="btn btn-lg btn-primary mt-3 w-100" type="submit">Publicar</button> 
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        
    </div>

    
    <?php
    include 'scripts.html'
    ?>
    <script>
        CKEDITOR.replace( 'editor1' )
    </script>

</body>
</html>

