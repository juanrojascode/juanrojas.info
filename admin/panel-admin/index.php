<?php session_start();
    if( isset($_SESSION['admin'])){

        include_once '../core/conexion.php';

        //LEER TABLA
        $sql_leer = 'SELECT * FROM articles';
        $gsent = $pdo->prepare($sql_leer);
        $gsent->execute();
        $resultado = $gsent->fetchAll();

    }else{
        header('location:../index');
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <?php
    include 'head.html'
    ?>

    <title>JuanRojas || Panel Administrativo</title>

    <script src="../ckeditor/ckeditor.js"></script>

</head>
<body>
    <div class="wrapper">

        <?php
        include 'nav.html'
        ?>
        <section class="content">
            <div class="main-content container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-table publishContent card-border-color card-border-color-primary">
                            <div class="card-header">Contenido publicado</div>
                            <div class="card-body">
                                <table class="table cPublicado table-striped">
                                    <thead>
                                        <tr>
                                            <th>Título</th>
                                            <th>Creación</th>
                                            <th>Creado por</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($resultado as $dato):
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $dato['title_node']; ?>
                                            </td>
                                            <td>
                                                <?php echo $dato['fecha_publicacion']; ?>
                                            </td>
                                            <td>
                                                Autor
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <a class="btn btn-secondary" href="editar?id=<?php echo $dato['id']; ?>">Editar</a>
                                                    <a class="btn btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-angle-down"></i>
                                                    </a>
                                                    <div class="dropdown-menu">
                                                        <a data-id="<?php echo $dato['id']; ?>" class="dropdown-item" data-toggle="modal" data-target="#modalEliminar">Eliminar</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <div class="modal effect modal-warning" id="modalEliminar" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fas fa-exclamation-triangle display-3"></i>
                        <h3 class="mb-4">¡Atención!</h3>
                        <p>Está tratando de borrar un artículo de la base de datos
                        <br>¿está seguro de hacerlo?</p>
                        <div class="mt-5">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancelar</button>
                            <a class="btn btn-secondary aceptar" href="">Aceptar</a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    
    <?php
    include 'scripts.html'
    ?>

</body>
</html>