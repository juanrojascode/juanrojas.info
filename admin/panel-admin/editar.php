<?php session_start();
    if( isset($_SESSION['admin'])){

        include_once '../core/conexion.php';

        $id = $_GET['id'];
            
        $sql_unico = 'SELECT * FROM articles WHERE id=?';
        $gsent_unico = $pdo->prepare($sql_unico);
        $gsent_unico->execute(array($id));
        $resultado_unico = $gsent_unico->fetch();

        if($_POST){
            $idEdit = $_POST['id'];
            $articleTitle = $_POST['title_node'];
            $articleContent = $_POST['content_node'];
            $articleTemplate = $_POST['select_template'];
            $articleDate = $_POST['fecha_creacion'];
            
            if( $_FILES['img']['name'] != null ){
                $articleImage=$_FILES['img']['name'];
                $ruta=$_FILES['img']['tmp_name'];
                $destino="../../images/articles/".$articleImage;
                $destinoHome="images/articles/".$articleImage ;
                
                if(copy($ruta,$destino)){
                    $sql_agregar = 'UPDATE articles SET title_node=?,content_node=?,select_template=?,ruta_imagen=?,ruta_imagen_home=?,fecha_publicacion=? WHERE id=?';
                    $sentencia_agregar = $pdo->prepare($sql_agregar);
                    $sentencia_agregar->execute(array($articleTitle,$articleContent,$articleTemplate,$destino,$destinoHome,$articleDate,$idEdit));
                }
            }else{
                $sql_editar = 'UPDATE articles SET title_node=?,content_node=?,select_template=?,fecha_publicacion=? WHERE id=?';
                    $sentencia_editar = $pdo->prepare($sql_editar);
                    $sentencia_editar->execute(array($articleTitle,$articleContent,$articleTemplate,$articleDate,$idEdit));
            }

            $sentencia_editar = null;
            $pdo = null;
            header('location:index');
        }

    }else{
        header('location:../index');
    }
   
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php
    include 'head.html'
    ?>
    <title>Editar Artículo || Panel Administrativo</title>

    <script src="../ckeditor/ckeditor.js"></script>
    

</head>
<body>
    <div class="wrapper">

        <?php
        include 'nav.html'
        ?>

        <section class="content">
            <div class="main-content container-fluid">                   
                <form enctype="multipart/form-data" method="POST">
                    <div class="form-row">
                        <div class="card col-12 col-md-9 card-border-color card-border-color-primary">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Título <span class="requerido">*</span></label>
                                    <input class="form-control" name="title_node" rows="1" value="<?php echo $resultado_unico['title_node']; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Contenido</label>
                                    <textarea name="content_node" id="editor1">
                                        <?php echo $resultado_unico['content_node']; ?>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card col-12 col-md-3 card-border-color card-border-color-primary">
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="mr-2 align-self-start">Tipo de Plantilla:</label>
                                    <div class="select-template mt-3 ml-2">
                                        <?php 
                                            if ( $resultado_unico['select_template'] == 6) {
                                                echo (' <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="6" checked>
                                                            <label class="form-check-label"><img src="images/layout1.svg" alt="Template grande" width="45px"></label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="4">
                                                            <label class="form-check-label"><img src="images/layout2.svg" alt="Template mediano" width="45px"></label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="3">
                                                            <label class="form-check-label"><img src="images/layout3.svg" alt="Template pequeño" width="45px"></label>
                                                        </div>');
                                            }if ( $resultado_unico['select_template'] == 4) {
                                                echo (' <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="6">
                                                            <label class="form-check-label"><img src="images/layout1.svg" alt="Template grande" width="45px"></label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="4" checked>
                                                            <label class="form-check-label"><img src="images/layout2.svg" alt="Template mediano" width="45px"></label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="3">
                                                            <label class="form-check-label"><img src="images/layout3.svg" alt="Template pequeño" width="45px"></label>
                                                        </div>');
                                            }if ( $resultado_unico['select_template'] == 3) {
                                                echo (' <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="6">
                                                            <label class="form-check-label"><img src="images/layout1.svg" alt="Template grande" width="45px"></label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="4">
                                                            <label class="form-check-label"><img src="images/layout2.svg" alt="Template mediano" width="45px"></label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="select_template" value="3" checked>
                                                            <label class="form-check-label"><img src="images/layout3.svg" alt="Template pequeño" width="45px"></label>
                                                        </div>');
                                            }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Seleccionar imagen:</label> 
                                    <input type="file" name="img" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="imagen">Imagen actual:</label><br>
                                    <img src="<?php echo $resultado_unico['ruta_imagen']; ?>" alt="" width="120px" height="auto">
                                </div>
                                <div class="form-group">
                                    <label>Fecha de publicación:</label>
                                    <?php 
                                        date_default_timezone_set('America/Bogota');
                                        echo '<input type="date" class="form-control" value="'.$resultado_unico['fecha_publicacion'].'" name="fecha_creacion" id="date">';
                                    ?>
                                </div>
                                <input type="hidden" value="<?php echo $resultado_unico['id']; ?>" name="id">
                                <button class="btn btn-lg btn-success mt-3 w-100" type="submit">Guardar</button>
                            </div>
                        </div>   
                    </div>
                </form>
            </div>
        </section>

    </div>

    
    <?php
    include 'scripts.html'
    ?>
    <script>
        CKEDITOR.replace( 'editor1' )
    </script>

</body>
</html>